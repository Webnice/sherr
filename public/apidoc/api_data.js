define({ "api": [
  {
    "type": "get",
    "url": "/api/category",
    "title": "List categories",
    "group": "Category",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n     \"status\": \"success\",\n     \"error_code\": 0,\n     \"message\": null,\n     \"data\":[\n         {\n              \"id\": 1,\n              \"name\": \"test1\",\n              \"icon\": \"/img/category/1.svg\"\n         },\n         {}..\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/CategoryDoc.php",
    "groupTitle": "Category",
    "name": "GetApiCategory",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/category"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/sub-category",
    "title": "List sub-categories",
    "group": "Category",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>{field_name}:{value}</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "?search=category_id:4",
          "type": "string"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"status\": \"success\",\n    \"error_code\": 0,\n    \"message\": null,\n    \"data\": [\n        {\n            \"id\": 1,\n            \"name\": \"Auto\",\n            \"category_id\": 4\n        },\n        {\n            \"id\": 2,\n            \"name\": \"Пила\",\n            \"category_id\": 5\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/CategoryDoc.php",
    "groupTitle": "Category",
    "name": "GetApiSubCategory",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/sub-category"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/orders",
    "title": "Get list",
    "group": "Orders",
    "version": "1.0.0",
    "examples": [
      {
        "title": "Conducting research in the repository::",
        "content": "?search=title:John Doe;category:4",
        "type": "string"
      },
      {
        "title": "By default RequestCriteria makes its queries using the OR comparison operator for each query parameter:",
        "content": "In order for it to query using the AND, pass the searchJoin parameter as shown below:\n?search=title:John Doe;category:4&searchJoin=and",
        "type": "string"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": \"success\",\n     \"error_code\": 0,\n     \"message\": null,\n     \"data\":[\n         {\n             \"id\": 1,\n             \"user_id\": 8,\n             \"title\": \"test\",\n             \"description\": null,\n             \"latitude\": \"50.471840\",\n             \"longitude\": \"30.402720\",\n             \"type\": \"private\",\n             \"category_id\": 4,\n             \"sub_category_id\": 1,\n             \"cost_day\": 100,\n             \"safe_deal\": 1,\n             \"insurance\": 1,\n             \"sum_insurance\": 2000,\n             \"request_passport\": 0,\n             \"created_at\": \"2020-03-09 17:08:03\",\n             \"updated_at\": \"2020-03-09 17:08:03\",\n             \"orderPhotos\": {\"data\": [\n                 {\n                     id\": 1,\n                     \"image\": \"/storage/order/gNXtKwMkMUfrumrIp0mknDOhC2CVghe4p52NVlWI.png\"\n                 }...\n             ]}\n         },\n         {...}\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/OrderDoc.php",
    "groupTitle": "Orders",
    "name": "GetApiOrders",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/orders"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/orders/{id}",
    "title": "Get order",
    "group": "Orders",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": \"success\",\n     \"error_code\": 0,\n     \"message\": null,\n     \"data\":{\n         \"id\": 1,\n         \"user_id\": 8,\n         \"title\": \"test\",\n         \"description\": null,\n         \"latitude\": \"50.471840\",\n         \"longitude\": \"30.402720\",\n         \"type\": \"private\",\n         \"category_id\": 4,\n         \"sub_category_id\": 1,\n         \"cost_day\": 100,\n         \"safe_deal\": 1,\n         \"insurance\": 1,\n         \"sum_insurance\": 2000,\n         \"request_passport\": 0,\n         \"created_at\": \"2020-03-09 17:08:03\",\n         \"updated_at\": \"2020-03-09 17:08:03\",\n         \"orderPhotos\": {\"data\": [\n                     {\n                         id\": 1,\n                         \"image\": \"/storage/order/gNXtKwMkMUfrumrIp0mknDOhC2CVghe4p52NVlWI.png\"\n                     }...\n                 ]}\n         }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Unprocessable-Response:",
          "content": "HTTP/1.1 404 Unprocessable Entity\n{\n     \"status\": \"error\",\n     \"error_code\": 404,\n     \"message\": \"Order not found\",\n     \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/OrderDoc.php",
    "groupTitle": "Orders",
    "name": "GetApiOrdersId",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/orders/{id}"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/orders",
    "title": "Create order",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Bearer",
            "description": "<p>{token}</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n     \"Content-Type: multipart/form-data\",\n     \"Authorization\": \"Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg\"\n}",
          "type": "json"
        }
      ]
    },
    "group": "Orders",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Заголовок аренды</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Описание аренды</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "latitude",
            "description": "<p>Latitude Координаты широты</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "longitude",
            "description": "<p>Longitude Координаты долготы</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "private",
              "business"
            ],
            "optional": false,
            "field": "type",
            "description": "<p>Тип аренды</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "category_id",
            "description": "<p>Категория</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "sub_category_id",
            "description": "<p>Подкатегория</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cost_day",
            "description": "<p>Цена аренды за день</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "0",
              "1"
            ],
            "optional": false,
            "field": "safe_deal",
            "description": "<p>Безопасная сделка 0-нет/1-да</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "allowedValues": [
              "0",
              "1"
            ],
            "optional": false,
            "field": "insurance",
            "description": "<p>Застраховать товар, 0-нет/1-да</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "sum_insurance",
            "description": "<p>Сумма строхования</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "allowedValues": [
              "0",
              "1"
            ],
            "optional": false,
            "field": "request_passport",
            "description": "<p>Запрашивать паспорт или нет.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photos[]",
            "description": "<p>фотографии товара</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"title\":\"test\",\n  \"description\":\"test test test test test test test \",\n  \"user_id\": \"8\",\n  \"latitude\":\"50.471840\",\n  \"longitude\":\"30.402720\",\n  \"type\":\"private\",\n  \"category_id\":\"4\",\n  \"sub_category_id\":\"1\",\n  \"cost_day\":\"100\",\n  \"safe_deal\":\"1\",\n  \"insurance\":\"1\",\n  \"sum_insurance\":\"2000\",\n  \"request_passport\":\"0\"\n  \"photos[]\": \"files\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": \"success\",\n     \"error_code\": 0,\n     \"message\": null,\n     \"data\":{\n         \"title\": \"test23\",\n         \"latitude\": \"51.471840\",\n         \"longitude\": \"32.402720\",\n         \"type\": \"private\",\n         \"category_id\": \"4\",\n         \"sub_category_id\": \"1\",\n         \"cost_day\": \"100\",\n         \"safe_deal\": \"1\",\n         \"insurance\": \"1\",\n         \"sum_insurance\": \"2000\",\n         \"request_passport\": \"0\",\n         \"user_id\": 1,\n         \"updated_at\": \"2020-03-21 15:51:14\",\n         \"created_at\": \"2020-03-21 15:51:14\",\n         \"id\": 3\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Unprocessable-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n     \"status\": \"error\",\n     \"error_code\": 422,\n     \"data\" => null,\n     \"message\": \"Request body invalid\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/OrderDoc.php",
    "groupTitle": "Orders",
    "name": "PostApiOrders",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/orders"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/orders/tenant",
    "title": "To rent",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Bearer",
            "description": "<p>{token}</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n     \"Content-Type: application/json\",\n     \"Authorization\": \"Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg\"\n}",
          "type": "json"
        }
      ]
    },
    "group": "Orders",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order_id",
            "description": "<p>Идентификатор оренды</p>"
          },
          {
            "group": "Parameter",
            "type": "DateTime",
            "optional": false,
            "field": "start_date",
            "description": "<p>Дата начала аренды</p>"
          },
          {
            "group": "Parameter",
            "type": "DateTime",
            "optional": false,
            "field": "end_date",
            "description": "<p>Дата окончания аренды</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"order_id\":\"1\",\n  \"start_date\":\"2020-03-29 13:47:20\",\n  \"end_date\":\"2020-04-05 13:50:20\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": \"success\",\n     \"error_code\": 0,\n     \"message\": null,\n     \"data\":{\n         \"order_id\": 1,\n         \"start_date\": \"2020-03-29 13:47:20\",\n         \"end_date\": \"2020-04-05 13:50:20\",\n         \"user_id\": 1,\n         \"updated_at\": \"2020-03-29 10:59:33\",\n         \"created_at\": \"2020-03-29 10:59:33\",\n         \"id\": 1\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Unprocessable-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n     \"status\": \"error\",\n     \"error_code\": 422,\n     \"data\" => null,\n     \"message\": \"Request body invalid\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/OrderDoc.php",
    "groupTitle": "Orders",
    "name": "PostApiOrdersTenant",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/orders/tenant"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/user/details",
    "title": "Current user information",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Bearer",
            "description": "<p>{token}</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg\"\n}",
          "type": "json"
        }
      ]
    },
    "group": "User",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n     \"status\": \"success\",\n     \"error_code\": 0,\n     \"message\": null,\n     \"data\": {\n         \"id\": 1,\n         \"name\": \"Esmeralda Zulauf\",\n         \"email\": \"fay33@example.com\",\n         \"email_verified_at\": \"2020-02-13 21:30:36\",\n         \"created_at\": \"2020-02-13 21:30:36\",\n         \"updated_at\": \"2020-02-13 21:30:36\"\n     }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/UsersDoc.php",
    "groupTitle": "User",
    "name": "GetApiUserDetails",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/user/details"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/user/avatar",
    "title": "Upload avatar",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": ""
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type: multipart/form-data\"\n  \"Authorization\": \"Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg\"\n}",
          "type": "json"
        }
      ]
    },
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "avatar",
            "description": "<p>image.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": \"success\",\n     \"error_code\": 0,\n     \"message\": null,\n     \"data\": \"/storage/users/7PKDJMroAfY9spo0WS07xVAuH9ZtVJLslQruaXja.jpeg\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Unprocessable-Response:",
          "content": "HTTP/1.1 400 Unprocessable Entity\n{\n     \"status\": \"error\",\n     \"error_code\": 404,\n     \"message\": \"File not found\",\n     \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/UsersDoc.php",
    "groupTitle": "User",
    "name": "PostApiUserAvatar",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/user/avatar"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/user/register",
    "title": "User registration",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "defaultValue": "application/json",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type: application/json\"\n}",
          "type": "json"
        }
      ]
    },
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "..80",
            "optional": false,
            "field": "name",
            "description": "<p>User name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "6..",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n   \"name\": \"test name\",\n   \"email\": \"example@example.com\",\n   \"password\": \"password\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": \"success\",\n   \"error_code\": 0,\n   \"message\": null,\n   \"data\" : {\n         \"name\": \"webnice24\",\n         \"email\": \"webnice24.mn@gmail.com\",\n         \"updated_at\": \"2020-02-16 16:42:00\",\n         \"created_at\": \"2020-02-16 16:42:00\",\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Unprocessable-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"status\": \"error\",\n   \"error_code\": 422,\n   \"message\": \"Request body invalid\",\n   \"data\" : {\n     \"name\":[\n         \"validation.required\",\n         \"validation.max.string\",\n         \"validation.required\"\n     ],\n     \"email\":[\n         \"validation.email\",\n         \"validation.unique\",\n         \"validation.required\"\n     ],\n     \"password\":[\n         \"validation.required\",\n         \"validation.regex\" не соответствует (Содержит заглавную букву, Одна цифра, Минимум 6 символов)\n     ]\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/UsersDoc.php",
    "groupTitle": "User",
    "name": "PostApiUserRegister",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/user/register"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/user/verify",
    "title": "Verify",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": ""
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type: multipart/form-data\"\n  \"Authorization\": \"Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg\"\n}",
          "type": "json"
        }
      ]
    },
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "passport[]",
            "description": "<p>image.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 OK\n{\n     \"status\": \"success\",\n     \"error_code\": 0,\n     \"message\": null,\n     \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Unprocessable-Response:",
          "content": "HTTP/1.1 400 Unprocessable Entity\n{\n     \"status\": \"error\",\n     \"error_code\": 404,\n     \"message\": \"File not found\",\n     \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/UsersDoc.php",
    "groupTitle": "User",
    "name": "PostApiUserVerify",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/api/user/verify"
      }
    ]
  },
  {
    "type": "post",
    "url": "/oauth/token",
    "title": "Get oauth token",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "defaultValue": "application/x-www-form-urlencoded",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Content-Type: application/x-www-form-urlencoded\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>User authorization</p>",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grant_type",
            "defaultValue": "password",
            "description": "<p>Oauth type (transfer (password)).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "client_id",
            "description": "<p>Client ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Client secret key.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "scope",
            "defaultValue": "*",
            "description": "<p>User scope (transfer (*)).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "Form data:\n  grant_type=password&client_id=2&client_secret=uhgGIUGugvUIGVuVugUGuvy78t7g97tyGbvuHUG87&username=example@example.com&password=password&scope=*,",
          "type": "x-www-form-urlencoded"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"token_type\": \"Bearer\",\n  \"expires_in\": 31622400,\n  \"access_token\": \"1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg\",\n  \"refresh_token\": \"cbf8aa9259344114c26eabb19caaf8e57c2c41e4fd58ef7a5e4a83b95cf6d025f13cea7549c\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Bad-Request-Response:",
          "content": "HTTP/1.1 400 BAD REQUEST\n{\n  \"error\": \"invalid_grant\",\n  \"error_description\": \"The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.\",\n  \"hint\": \"\",\n  \"message\": \"The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.\"\n}",
          "type": "json"
        },
        {
          "title": "Unauthorized-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"error\": \"invalid_client\",\n  \"error_description\": \"Client authentication failed\",\n  \"message\": \"Client authentication failed\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/ApiDoc/UsersDoc.php",
    "groupTitle": "User",
    "name": "PostOauthToken",
    "sampleRequest": [
      {
        "url": "http://143.198.125.4/oauth/token"
      }
    ]
  }
] });
