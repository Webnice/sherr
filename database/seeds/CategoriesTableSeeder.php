<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Транспорт',
                'icon' => '/img/category/1.svg'
            ],
            [
                'name' => 'Техника',
                'icon' => '/img/category/2.svg'
            ],
            [
                'name' => 'Жилье',
                'icon' => '/img/category/3.svg'
            ],
            [
                'name' => 'Игры',
                'icon' => '/img/category/4.svg'
            ],
            [
                'name' => 'Инструмент',
                'icon' => '/img/category/5.svg'
            ],
            [
                'name' => 'Детский мир',
                'icon' => '/img/category/6.svg'
            ],
            [
                'name' => 'Мода',
                'icon' => '/img/category/7.svg'
            ],
            [
                'name' => 'Хобби',
                'icon' => '/img/category/8.svg'
            ],
            [
                'name' => 'Спорт',
                'icon' => '/img/category/9.svg'
            ],
            [
                'name' => 'Музыка',
                'icon' => '/img/category/10.svg'
            ],
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
