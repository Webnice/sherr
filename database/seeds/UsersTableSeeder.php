<?php

use \Illuminate\Database\Seeder;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run
     */
    public function run()
    {
        factory(App\Models\User::class, 3)->create();
    }
}