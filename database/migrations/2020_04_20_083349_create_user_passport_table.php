<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPassportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();
            Schema::create('user_passport', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->string('file');
                $table->timestamps();
            });

            Schema::table('users', function (Blueprint $table) {
                $table->smallInteger('verify')->after('avatar')->default(0);
            });

            DB::commit();
        } catch (PDOException $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_passport');

        Schema::create('users', function (Blueprint $table) {
            $table->dropColumn('verify');
        });
    }
}
