<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('latitude');
            $table->string('longitude');
            $table->enum('type', ['private', 'business']);
            $table->unsignedInteger('category_id');
            $table->integer('sub_category_id')->nullable()->unsigned();
            $table->integer('cost_day');
            $table->smallInteger('safe_deal')->default(0);
            $table->smallInteger('insurance')->default(0);
            $table->integer('sum_insurance')->nullable();
            $table->smallInteger('request_passport')->default(0);
            $table->timestamps();


            $table->foreign('sub_category_id')->references('id')->on('sub_categories');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_sub_category_id_foreign');
            $table->dropForeign('orders_user_id_foreign');
            $table->dropForeign('orders_category_id_foreign');

            $table->drop();
        });
    }
}
