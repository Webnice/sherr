<?php

namespace App\Transformers;

use App\Models\OrderPhotos;
use League\Fractal\TransformerAbstract;

/**
 * Class OrderPhotosTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrderPhotosTransformer extends TransformerAbstract
{
    /**
     * Transform the Order entity.
     *
     * @param \App\Models\OrderPhotos $model
     *
     * @return array
     */
    public function transform(OrderPhotos $model)
    {
        return [
            'id' => (int) $model->id,
            'image' => (string) '/storage/' . $model->image
        ];
    }
}
