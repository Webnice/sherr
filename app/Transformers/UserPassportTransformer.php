<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\UserPassport;

/**
 * Class UserPassportTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserPassportTransformer extends TransformerAbstract
{
    /**
     * Transform the UserPassport entity.
     *
     * @param \App\Models\UserPassport $model
     *
     * @return array
     */
    public function transform(UserPassport $model)
    {
        return [
            'file' => '/storage/' . $model->file
        ];
    }
}
