<?php

namespace App\Transformers;

use App\Models\Category;
use League\Fractal\TransformerAbstract;

/**
 * Class CategoryTransformer.
 *
 * @package namespace App\Transformers;
 */
class CategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the SubCategory entity.
     *
     * @param \App\Models\Category $model
     *
     * @return array
     */
    public function transform(Category $model): array
    {
        return [
            'id' => (int) $model->id,
            'name' => (string) $model->name,
            'icon' => (string) $model->icon
        ];
    }
}
