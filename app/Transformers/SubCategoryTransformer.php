<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\SubCategory;

/**
 * Class SubCategoryTransformer.
 *
 * @package namespace App\Transformers;
 */
class SubCategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the SubCategory entity.
     *
     * @param \App\Models\SubCategory $model
     *
     * @return array
     */
    public function transform(SubCategory $model): array
    {
        return [
            'id' => (int) $model->id,
            'name' => (string) $model->name,
            'category_id' => (int) $model->category_id
        ];
    }
}
