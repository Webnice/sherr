<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Order;

/**
 * Class OrderTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrderTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = ['orderPhotos'];

    /**
     * Transform the Order entity.
     *
     * @param \App\Models\Order $model
     *
     * @return array
     */
    public function transform(Order $model)
    {
        return $model->toArray();
    }

    /**
     * @param Order $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeOrderPhotos(Order $model)
    {
        if ($model->orderPhotos) {
            return $this->collection($model->orderPhotos, new OrderPhotosTransformer());
        } else {
            return null;
        }
    }
}
