<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * optional user profile data
     */
    protected $defaultIncludes = ['userPassport'];

    /**
     * Transform the User entity.
     * @param User $model
     *
     * @return array
     */
    public function transform(User $model): array
    {
        return [
            'id' => (int) $model->id,
            'name' => (string) $model->name,
            'email' => (string) $model->email,
            'avatar' => (string) '/storage/' . $model->avatar,
            'verify' => (int) $model->verify,
        ];
    }

    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeUserPassport(User $model)
    {
        if ($model->userPassport) {
            return $this->collection($model->userPassport, new UserPassportTransformer());
        } else {
            return null;
        }
    }
}
