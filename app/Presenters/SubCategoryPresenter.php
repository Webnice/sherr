<?php

namespace App\Presenters;

use App\Transformers\SubCategoryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SubCategoryPresenter.
 *
 * @package namespace App\Presenters;
 */
class SubCategoryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SubCategoryTransformer();
    }
}
