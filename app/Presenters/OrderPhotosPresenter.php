<?php

namespace App\Presenters;

use App\Transformers\OrderPhotosTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrderPhotosPresenter.
 *
 * @package namespace App\Presenters;
 */
class OrderPhotosPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrderPhotosTransformer();
    }
}
