<?php

namespace App\Presenters;

use App\Transformers\UserPassportTransformer;
use Illuminate\Contracts\Support\Arrayable;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class UserPassportPresenter.
 *
 * @package namespace App\Presenters;
 */
class UserPassportPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserPassportTransformer();
    }
}
