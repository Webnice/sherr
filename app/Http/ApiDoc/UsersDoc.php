<?php

/**
 * @api {post} /oauth/token Get oauth token
 * @apiHeader {String} Content-Type=application/x-www-form-urlencoded
 * @apiHeaderExample {json} Header-Example:
 * {
 *   "Content-Type: application/x-www-form-urlencoded"
 * }
 * @apiDescription User authorization
 * @apiGroup User
 * @apiVersion 1.0.0
 * @apiParam {String} grant_type=password Oauth type (transfer (password)).
 * @apiParam {Number} client_id Client ID.
 * @apiParam {String} client_secret Client secret key.
 * @apiParam {String} username User email.
 * @apiParam {String} password User password.
 * @apiParam {String} scope=* User scope (transfer (*)).
 * @apiParamExample {x-www-form-urlencoded} Request-Example:
 * Form data:
 *   grant_type=password&client_id=2&client_secret=uhgGIUGugvUIGVuVugUGuvy78t7g97tyGbvuHUG87&username=example@example.com&password=password&scope=*,
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *   "token_type": "Bearer",
 *   "expires_in": 31622400,
 *   "access_token": "1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg",
 *   "refresh_token": "cbf8aa9259344114c26eabb19caaf8e57c2c41e4fd58ef7a5e4a83b95cf6d025f13cea7549c"
 * }
 * @apiErrorExample {json} Bad-Request-Response:
 * HTTP/1.1 400 BAD REQUEST
 * {
 *   "error": "invalid_grant",
 *   "error_description": "The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client.",
 *   "hint": "",
 *   "message": "The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client."
 * }
 * @apiErrorExample {json} Unauthorized-Response:
 * HTTP/1.1 401 Unauthorized
 * {
 *   "error": "invalid_client",
 *   "error_description": "Client authentication failed",
 *   "message": "Client authentication failed"
 * }
 */

/**
 * @api {post} /api/user/register User registration
 * @apiHeader {String} Content-Type=application/json
 * @apiHeaderExample {json} Header-Example:
 * {
 *   "Content-Type: application/json"
 * }
 * @apiGroup User
 * @apiVersion 1.0.0
 * @apiParam {String{..80}} name User name.
 * @apiParam {String} email User email.
 * @apiParam {String{6..}} password User password.
 * @apiParamExample {json} Request-Example:
 * {
 *    "name": "test name",
 *    "email": "example@example.com",
 *    "password": "password"
 * }
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "status": "success",
 *    "error_code": 0,
 *    "message": null,
 *    "data" : {
 *          "name": "webnice24",
 *          "email": "webnice24.mn@gmail.com",
 *          "updated_at": "2020-02-16 16:42:00",
 *          "created_at": "2020-02-16 16:42:00",
 *    }
 * }
 *
 * @apiErrorExample {json} Unprocessable-Response:
 * HTTP/1.1 422 Unprocessable Entity
 * {
 *    "status": "error",
 *    "error_code": 422,
 *    "message": "Request body invalid",
 *    "data" : {
 *      "name":[
 *          "validation.required",
 *          "validation.max.string",
 *          "validation.required"
 *      ],
 *      "email":[
 *          "validation.email",
 *          "validation.unique",
 *          "validation.required"
 *      ],
 *      "password":[
 *          "validation.required",
 *          "validation.regex" не соответствует (Содержит заглавную букву, Одна цифра, Минимум 6 символов)
 *      ]
 *    }
 * }
 */

/**
 * @api {get} /api/user/details Current user information
 * @apiHeader {String} Authorization=Bearer {token}
 * @apiHeaderExample {json} Header-Example:
 * {
 *   "Authorization": "Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg"
 * }
 * @apiGroup User
 * @apiVersion 1.0.0
 * @apiSuccessExample {json} Success-Response:
 * {
 *      "status": "success",
 *      "error_code": 0,
 *      "message": null,
 *      "data": {
 *          "id": 1,
 *          "name": "Esmeralda Zulauf",
 *          "email": "fay33@example.com",
 *          "email_verified_at": "2020-02-13 21:30:36",
 *          "created_at": "2020-02-13 21:30:36",
 *          "updated_at": "2020-02-13 21:30:36"
 *      }
 * }
 */

/**
 * @api {post} /api/user/avatar Upload avatar
 * @apiHeader {String} Content-Type
 * @apiHeader {String} Authorization
 * @apiHeaderExample {json} Header-Example:
 * {
 *   "Content-Type: multipart/form-data"
 *   "Authorization": "Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg"
 * }
 * @apiGroup User
 * @apiVersion 1.0.0
 * @apiParam {File} avatar image.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": "success",
 *      "error_code": 0,
 *      "message": null,
 *      "data": "/storage/users/7PKDJMroAfY9spo0WS07xVAuH9ZtVJLslQruaXja.jpeg"
 * }
 * @apiErrorExample {json} Unprocessable-Response:
 * HTTP/1.1 400 Unprocessable Entity
 * {
 *      "status": "error",
 *      "error_code": 404,
 *      "message": "File not found",
 *      "data": null
 * }
 */

/**
 * @api {post} /api/user/verify Verify
 * @apiHeader {String} Content-Type
 * @apiHeader {String} Authorization
 * @apiHeaderExample {json} Header-Example:
 * {
 *   "Content-Type: multipart/form-data"
 *   "Authorization": "Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg"
 * }
 * @apiGroup User
 * @apiVersion 1.0.0
 * @apiParam {File} passport[] image.
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 204 OK
 * {
 *      "status": "success",
 *      "error_code": 0,
 *      "message": null,
 *      "data": null
 * }
 * @apiErrorExample {json} Unprocessable-Response:
 * HTTP/1.1 400 Unprocessable Entity
 * {
 *      "status": "error",
 *      "error_code": 404,
 *      "message": "File not found",
 *      "data": null
 * }
 */
