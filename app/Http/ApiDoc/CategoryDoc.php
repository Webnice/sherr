<?php

/**
 * @api {get} /api/category List categories
 * @apiGroup Category
 * @apiVersion 1.0.0
 * @apiSuccessExample {json} Success-Response:
 * {
 *      "status": "success",
 *      "error_code": 0,
 *      "message": null,
 *      "data":[
 *          {
 *               "id": 1,
 *               "name": "test1",
 *               "icon": "/img/category/1.svg"
 *          },
 *          {}..
 *      ]
 * }
 */

/**
 * @api {get} /api/sub-category List sub-categories
 * @apiGroup Category
 * @apiVersion 1.0.0
 *
 * @apiParam {String} [search] {field_name}:{value}
 * @apiParamExample {string} Request-Example:
 *                  ?search=category_id:4
 *
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "status": "success",
 *     "error_code": 0,
 *     "message": null,
 *     "data": [
 *         {
 *             "id": 1,
 *             "name": "Auto",
 *             "category_id": 4
 *         },
 *         {
 *             "id": 2,
 *             "name": "Пила",
 *             "category_id": 5
 *         }
 *     ]
 * }
 */
