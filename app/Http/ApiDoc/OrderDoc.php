<?php

/**
 * @api {post} /api/orders Create order
 *
 * @apiHeader {String} Content-Type application/json
 * @apiHeader {String} Authorization=Bearer {token}
 *
 * @apiHeaderExample {json} Header-Example:
 * {
 *      "Content-Type: multipart/form-data",
 *      "Authorization": "Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg"
 * }
 *
 * @apiGroup Orders
 * @apiVersion 1.0.0
 *
 * @apiParam {String} title Заголовок аренды
 * @apiParam {String} [description] Описание аренды
 * @apiParam {Number} latitude Latitude Координаты широты
 * @apiParam {Number} longitude Longitude Координаты долготы
 * @apiParam {string=private,business} type Тип аренды
 * @apiParam {Number} category_id Категория
 * @apiParam {Number} [sub_category_id] Подкатегория
 * @apiParam {Number} cost_day Цена аренды за день
 * @apiParam {Number=0,1} safe_deal Безопасная сделка 0-нет/1-да
 * @apiParam {number=0,1} insurance Застраховать товар, 0-нет/1-да
 * @apiParam {Number} sum_insurance Сумма строхования
 * @apiParam {number=0,1} request_passport Запрашивать паспорт или нет.
 * @apiParam {String} photos[] фотографии товара
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "title":"test",
 *   "description":"test test test test test test test ",
 *   "user_id": "8",
 *   "latitude":"50.471840",
 *   "longitude":"30.402720",
 *   "type":"private",
 *   "category_id":"4",
 *   "sub_category_id":"1",
 *   "cost_day":"100",
 *   "safe_deal":"1",
 *   "insurance":"1",
 *   "sum_insurance":"2000",
 *   "request_passport":"0"
 *   "photos[]": "files"
 * }
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": "success",
 *      "error_code": 0,
 *      "message": null,
 *      "data":{
 *          "title": "test23",
 *          "latitude": "51.471840",
 *          "longitude": "32.402720",
 *          "type": "private",
 *          "category_id": "4",
 *          "sub_category_id": "1",
 *          "cost_day": "100",
 *          "safe_deal": "1",
 *          "insurance": "1",
 *          "sum_insurance": "2000",
 *          "request_passport": "0",
 *          "user_id": 1,
 *          "updated_at": "2020-03-21 15:51:14",
 *          "created_at": "2020-03-21 15:51:14",
 *          "id": 3
 *      }
 * }
 * @apiErrorExample {json} Unprocessable-Response:
 * HTTP/1.1 422 Unprocessable Entity
 * {
 *      "status": "error",
 *      "error_code": 422,
 *      "data" => null,
 *      "message": "Request body invalid"
 * }
 */

/**
 * @api {post} /api/orders/tenant To rent
 *
 * @apiHeader {String} Content-Type application/json
 * @apiHeader {String} Authorization=Bearer {token}
 *
 * @apiHeaderExample {json} Header-Example:
 * {
 *      "Content-Type: application/json",
 *      "Authorization": "Bearer 1X899GOuLn0CQxFs-zi-59WdO4ysL7UVfN2oSZ6xVPrFH4BeCaXpUpmXU9KZg"
 * }
 *
 * @apiGroup Orders
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} order_id Идентификатор оренды
 * @apiParam {DateTime} start_date Дата начала аренды
 * @apiParam {DateTime} end_date Дата окончания аренды
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "order_id":"1",
 *   "start_date":"2020-03-29 13:47:20",
 *   "end_date":"2020-04-05 13:50:20"
 * }
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": "success",
 *      "error_code": 0,
 *      "message": null,
 *      "data":{
 *          "order_id": 1,
 *          "start_date": "2020-03-29 13:47:20",
 *          "end_date": "2020-04-05 13:50:20",
 *          "user_id": 1,
 *          "updated_at": "2020-03-29 10:59:33",
 *          "created_at": "2020-03-29 10:59:33",
 *          "id": 1
 *      }
 * }
 * @apiErrorExample {json} Unprocessable-Response:
 * HTTP/1.1 422 Unprocessable Entity
 * {
 *      "status": "error",
 *      "error_code": 422,
 *      "data" => null,
 *      "message": "Request body invalid"
 * }
 */

/**
 * @api {get} /api/orders Get list
 *
 * @apiGroup Orders
 * @apiVersion 1.0.0
 *
 * @apiExample {string} Conducting research in the repository::
 *                      ?search=title:John Doe;category:4
 *
 * @apiExample {string} By default RequestCriteria makes its queries using the OR comparison operator for each query parameter:
 *                      In order for it to query using the AND, pass the searchJoin parameter as shown below:
 *                      ?search=title:John Doe;category:4&searchJoin=and
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": "success",
 *      "error_code": 0,
 *      "message": null,
 *      "data":[
 *          {
 *              "id": 1,
 *              "user_id": 8,
 *              "title": "test",
 *              "description": null,
 *              "latitude": "50.471840",
 *              "longitude": "30.402720",
 *              "type": "private",
 *              "category_id": 4,
 *              "sub_category_id": 1,
 *              "cost_day": 100,
 *              "safe_deal": 1,
 *              "insurance": 1,
 *              "sum_insurance": 2000,
 *              "request_passport": 0,
 *              "created_at": "2020-03-09 17:08:03",
 *              "updated_at": "2020-03-09 17:08:03",
 *              "orderPhotos": {"data": [
 *                  {
 *                      id": 1,
 *                      "image": "/storage/order/gNXtKwMkMUfrumrIp0mknDOhC2CVghe4p52NVlWI.png"
 *                  }...
 *              ]}
 *          },
 *          {...}
 *      ]
 * }

 */

/**
 * @api {get} /api/orders/{id} Get order
 *
 * @apiGroup Orders
 * @apiVersion 1.0.0
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": "success",
 *      "error_code": 0,
 *      "message": null,
 *      "data":{
 *          "id": 1,
 *          "user_id": 8,
 *          "title": "test",
 *          "description": null,
 *          "latitude": "50.471840",
 *          "longitude": "30.402720",
 *          "type": "private",
 *          "category_id": 4,
 *          "sub_category_id": 1,
 *          "cost_day": 100,
 *          "safe_deal": 1,
 *          "insurance": 1,
 *          "sum_insurance": 2000,
 *          "request_passport": 0,
 *          "created_at": "2020-03-09 17:08:03",
 *          "updated_at": "2020-03-09 17:08:03",
 *          "orderPhotos": {"data": [
 *                      {
 *                          id": 1,
 *                          "image": "/storage/order/gNXtKwMkMUfrumrIp0mknDOhC2CVghe4p52NVlWI.png"
 *                      }...
 *                  ]}
 *          }
 * }
 * @apiErrorExample {json} Unprocessable-Response:
 * HTTP/1.1 404 Unprocessable Entity
 * {
 *      "status": "error",
 *      "error_code": 404,
 *      "message": "Order not found",
 *      "data": null
 * }
 */
