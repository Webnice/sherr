<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderPhotos;
use App\Models\OrderTenant;
use App\Models\User;
use App\Repositories\Interfaces\OrderTenantRepository;
use App\Validators\OrderTenantValidator;
use Illuminate\Http\Request;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\Interfaces\OrderRepository;
use App\Validators\OrderValidator;
use \Auth;

/**
 * Class OrdersController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersController extends Controller
{
    /** @var OrderRepository  */
    protected $repository;

    /** @var OrderValidator  */
    protected $validator;

    /** @var OrderTenantRepository */
    protected $tenantRepository;

    /** @var OrderTenantValidator */
    protected $tenantValidator;

    /**
     * OrdersController constructor.
     *
     * @param OrderRepository $repository
     * @param OrderValidator $validator
     */
    public function __construct(
        OrderRepository $repository,
        OrderValidator $validator,
        OrderTenantRepository $tenantRepository,
        OrderTenantValidator $tenantValidator
    )
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->tenantRepository = $tenantRepository;
        $this->tenantValidator = $tenantValidator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $orders = $this->repository->all();

        return response()->json([
            'status' => 'success',
            'error_code' => 0,
            'message' => null,
            'data' => $orders['data']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ValidatorException
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->post())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $order = $this->repository->create(array_merge(
                $request->all(),
                ['user_id' => Auth::user()->id]
            ));

            if ($request->hasFile('photos')) {
                foreach ($request->photos as $passport) {
                    $path = $passport->store('order', 'public');

                    OrderPhotos::create([
                        'order_id' => $order['data']['id'],
                        'image' => $path
                    ]);
                }
            }

            return response()->json([
                'status' => 'success',
                'error_code' => 0,
                'message' => null,
                'data' => $order
            ]);
        } catch (ValidatorException $e) {
            return response()->json([
                'status' => 'error',
                'error_code' => 422,
                'data' => null,
                'message' => 'Request body invalid'
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $order = $this->repository->find($id);

            return response()->json([
                'status' => 'success',
                'error_code' => 0,
                'message' => null,
                'data' => $order['data']
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'error_code' => 404,
                'message' => 'Order not found',
                'data' => null
            ], 404);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tenant(Request $request) {
        try {
            $this->tenantValidator->with($request->post())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $orderTenant = $this->tenantRepository->create(array_merge(
                $request->all(),
                ['user_id' => Auth::user()->id]
            ));

            return response()->json([
                'status' => 'success',
                'error_code' => 0,
                'message' => null,
                'data' => $orderTenant
            ]);
        } catch (ValidatorException $e) {
            return response()->json([
                'status' => 'error',
                'error_code' => 422,
                'data' => null,
                'message' => 'Request body invalid'
            ], 422);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTenantStatus(Request $request, $id) {
        try {
            $this->tenantValidator->with($request->post())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            /** @var User $user */
            $user = Auth::user();
            /** @var OrderTenant $orderTenant */
            $orderTenant = $this->tenantRepository->find($id);

            if ($orderTenant->user_id == $user->id) {
                $orderTenant->user_status = $request->get('status');
            } else {
                $orderTenant->owner_status = $request->get('status');
            }

            $orderTenant->save();

            return response()->json([
                'status' => 'success',
                'error_code' => 201,
                'data' => null,
                'message' => null
            ], 201);

        }catch (ValidatorException $e) {
            return response()->json([
                'status' => 'error',
                'error_code' => 422,
                'data' => null,
                'message' => 'Request body invalid'
            ], 422);
        }
    }

    public function delete($id)
    {
        Order::where('id', $id)->delete();
    }

    public function chats()
    {
        return response()->json([
                'status' => 'success',
                'error_code' => 0,
                'data' => [
                    [
                        'id' => 1,
                        'user_id' => 2,
                        'last_message' => 'Test message',
                        'last_message_time' => '2020-03-21 15:51:14'
                    ]
                ],
                'message' => null
            ], 200);
    }

    public function chatsDetail($id)
    {
        return response()->json([
                'status' => 'success',
                'error_code' => 0,
                'data' => [
                    [
                        'id' => 1,
                        'user_id' => 2,
                        'message' => 'Test message',
                        'date_time' => '2020-03-21 15:51:14'
                    ],
                    [
                        'id' => 2,
                        'user_id' => 2,
                        'message' => 'Test message',
                        'date_time' => '2020-03-21 15:51:24'
                    ]
                ],
                'message' => null
            ], 200);
    }

    public function chat()
    {
            return response()->json([
                'status' => 'success',
                'error_code' => 0,
                'data' => [],
                'message' => null
            ], 200);
    }
}
