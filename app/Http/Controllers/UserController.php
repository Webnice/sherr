<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserPassport;
use App\Repositories\Interfaces\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

/**
 * Class RegisterController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        $user = $this->repository->find(Auth::user()->id);

        return response()->json([
            'status' => 'success',
            'error_code' => 0,
            'message' => null,
            'data' => $user['data']
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return response()->json(User::all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:80',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error_code' => 422,
                'message' => 'Request body invalid',
                'data' => $validator->errors()
            ], 422);
        }

        $user = User::create($request->all());

        return response()->json([
            'status' => 'success',
            'error_code' => 0,
            'message' => null,
            'data' => $user
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function avatar(Request $request)
    {
        if (!$request->hasFile('avatar')) {
            return response()->json([
                'status' => 'error',
                'error_code' => 404,
                'message' => "File not found",
                'data' => null
            ]);
        }

        $user = Auth::user();
        $oldFile = storage_path('app/public/' . $user->avatar);

        if (is_file($oldFile)) {
            unlink($oldFile);
        }

        $path = $request->avatar->store('users', 'public');
        $user->avatar = $path;
        $user->save();

        return response()->json([
            'status' => 'success',
            'error_code' => 0,
            'message' => null,
            'data' => '/storage/' . $user->avatar
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request)
    {
        if (!$request->hasFile('passport')) {
            return response()->json([
                'status' => 'error',
                'error_code' => 404,
                'message' => "File not found",
                'data' => null
            ]);
        }

        $user = Auth::user();

        foreach ($request->passport as $passport) {
            $path = $passport->store('passport', 'public');

            UserPassport::create([
                'user_id' => $user->id,
                'file' => $path
            ]);
        }

        return response()->json([
            'status' => 'success',
            'error_code' => 0,
            'message' => null,
            'data' => null
        ], 204);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, User $user)
    {
        if ($request->isMethod('put')) {
            $data = $request->all();

            $validator = Validator::make($data, [
                'name' => 'required|max:80',
                'email' => 'required|email',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            }

            $user->update($data);
        }

        return response()->json($user);
    }
}
