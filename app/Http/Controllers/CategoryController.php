<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\CategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $repository;

    public function __construct(CategoryRepository $repository){
        $this->repository = $repository;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'error_code' => 0,
            'message' => null,
            'data' => $this->repository->skipPresenter()->all(['id', 'name', 'icon'])
        ]);
    }
}
