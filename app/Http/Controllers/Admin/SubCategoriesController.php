<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\SubCategoryCreateRequest;
use App\Repositories\Interfaces\SubCategoryRepository;
use App\Validators\SubCategoryValidator;
use \Illuminate\Http\Response;
use \Illuminate\Http\JsonResponse;

/**
 * Class SubCategoriesController.
 *
 * @package namespace App\Http\Controllers\Admin;
 */
class SubCategoriesController extends Controller
{
    /**
     * @var SubCategoryRepository
     */
    protected $repository;

    /**
     * @var SubCategoryValidator
     */
    protected $validator;

    /**
     * SubCategoriesController constructor.
     *
     * @param SubCategoryRepository $repository
     * @param SubCategoryValidator $validator
     */
    public function __construct(SubCategoryRepository $repository, SubCategoryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $subCategories = $this->repository->all();

        return response()->json($subCategories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ValidatorException
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $subCategory = $this->repository->create($request->all());

            return response()->json( $subCategory);
        } catch (ValidatorException $e) {
            return response()->json(['message' => $e->getMessageBag()], 422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subCategory = $this->repository->find($id);

        return response()->json($subCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $subCategory = $this->repository->update($request->all(), $id);

            return response()->json($subCategory);
        } catch (ValidatorException $e) {
            return response()->json(['message' => $e->getMessageBag()], 422);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return response()->json([
            'message' => 'SubCategory deleted.',
            'deleted' => $deleted,
        ]);
    }
}
