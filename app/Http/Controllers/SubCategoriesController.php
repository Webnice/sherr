<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\SubCategoryRepository;
use App\Validators\SubCategoryValidator;

/**
 * Class SubCategoriesController.
 *
 * @package namespace App\Http\Controllers;
 */
class SubCategoriesController extends Controller
{
    /**
     * @var SubCategoryRepository
     */
    protected $repository;

    /**
     * @var SubCategoryValidator
     */
    protected $validator;

    /**
     * SubCategoriesController constructor.
     *
     * @param SubCategoryRepository $repository
     * @param SubCategoryValidator $validator
     */
    public function __construct(SubCategoryRepository $repository, SubCategoryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subCategories = $this->repository->skipPresenter()->all(['id', 'name', 'category_id']);

        return response()->json([
            'status' => 'success',
            'error_code' => 0,
            'message' => null,
            'data' => $subCategories
        ]);
    }
}
