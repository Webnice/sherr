<?php

namespace App\Providers;

use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\Interfaces\CategoryRepository;
use App\Repositories\Interfaces\OrderRepository;
use App\Repositories\Interfaces\OrderTenantRepository;
use App\Repositories\Interfaces\SubCategoryRepository;
use App\Repositories\Interfaces\UserRepository;
use App\Repositories\OrderRepositoryEloquent;
use App\Repositories\OrderTenantRepositoryEloquent;
use App\Repositories\SubCategoryRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(OrderRepository::class, OrderRepositoryEloquent::class);
        $this->app->bind(SubCategoryRepository::class, SubCategoryRepositoryEloquent::class);
        $this->app->bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        $this->app->bind(OrderTenantRepository::class, OrderTenantRepositoryEloquent::class);
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
    }
}
