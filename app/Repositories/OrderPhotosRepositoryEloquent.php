<?php

namespace App\Repositories;

use App\Models\OrderPhotos;
use App\Presenters\OrderPhotosPresenter;
use App\Repositories\Interfaces\OrderPhotosRepository;
use App\Validators\OrderPhotosValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class OrderTenantRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrderPhotosRepositoryEloquent extends BaseRepository implements OrderPhotosRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderPhotos::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return OrderPhotosValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return string
     */
    public function presenter()
    {
        return OrderPhotosPresenter::class;
    }
    
}
