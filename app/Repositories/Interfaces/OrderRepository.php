<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository.
 *
 * @package namespace App\Repositories\Interfaces;
 */
interface OrderRepository extends RepositoryInterface
{
    //
}
