<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SubCategoryRepository.
 *
 * @package namespace App\Repositories\Interfaces;
 */
interface SubCategoryRepository extends RepositoryInterface
{
    //
}
