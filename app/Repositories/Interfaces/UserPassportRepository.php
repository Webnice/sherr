<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserPassportRepository.
 *
 * @package namespace App\Repositories\Interfaces;
 */
interface UserPassportRepository extends RepositoryInterface
{
    //
}
