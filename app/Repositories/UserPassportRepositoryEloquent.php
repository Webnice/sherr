<?php

namespace App\Repositories;

use App\Presenters\UserPassportPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Interfaces\UserPassportRepository;
use App\Models\UserPassport;

/**
 * Class UserPassportRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserPassportRepositoryEloquent extends BaseRepository implements UserPassportRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserPassport::class;
    }

    /**
     * @return string
     */
    public function presenter() {
        return UserPassportPresenter::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
