<?php

namespace App\Repositories;

use App\Models\OrderTenant;
use App\Repositories\Interfaces\OrderTenantRepository;
use App\Validators\OrderTenantValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class OrderTenantRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrderTenantRepositoryEloquent extends BaseRepository implements OrderTenantRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderTenant::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return OrderTenantValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
