<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrderTenantValidator.
 *
 * @package namespace App\Validators;
 */
class OrderTenantValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'order_id' => 'required|exists:orders,id',
            'start_date' => 'required|date_format:Y-m-d H:i:s',
            'end_date' => 'required|date_format:Y-m-d H:i:s'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'status' => 'required|in:complete,cancel,arbitration,prolong,finish'
        ],
    ];
}
