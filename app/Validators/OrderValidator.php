<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OrderValidator.
 *
 * @package namespace App\Validators;
 */
class OrderValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title'            => 'required|string',
            'description'      => 'string',
            'latitude'         => 'required|string',
            'longitude'        => 'required|string',
            'type'             => 'required|in:private,business',
            'category_id'      => 'required|exists:categories,id',
            'sub_category_id'  => 'exists:sub_categories,id|nullable',
            'cost_day'         => 'required|numeric',
            'safe_deal'        => 'integer|nullable',
            'insurance'        => 'integer|nullable',
            'sum_insurance'    => 'numeric|nullable',
            'request_passport' => 'integer|nullable'
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
