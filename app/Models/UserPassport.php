<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserPassport.
 *
 * @package namespace App\Models;
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPassport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPassport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPassport query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $file
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPassport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPassport whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPassport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPassport whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserPassport whereUserId($value)
 * @property-read \App\Models\User $category
 */
class UserPassport extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'user_passport';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'file'
    ];

    /**
     * @return BelongsTo
     */
    public function category() {
        return $this->belongsTo(User::class);
    }
}
