<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * App\Models\OrderPhotos
 *
 * @property int $id
 * @property int $order_id
 * @property string $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderPhotos newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderPhotos newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderPhotos query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderPhotos whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderPhotos whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderPhotos whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderPhotos whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderPhotos whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderPhotos extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'order_photos';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /** @var array  */
    protected $fillable = [
        'order_id',
        'image'
    ];

    /**
     * @return belongsTo
     */
    public function order() {
        return $this->belongsTo(Order::class);
    }
}
