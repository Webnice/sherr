<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\OrderTenant
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string $start_date
 * @property string $end_date
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Order $order
 * @property string $status
 * @property string $user_status
 * @property string $owner_status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereOwnerStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderTenant whereUserStatus($value)
 */
class OrderTenant extends Model
{
    /**
     * @var string
     */
    protected $table = 'order_tenants';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'user_id',
        'start_date',
        'end_date'
    ];

    /**
     * @return hasOne
     */
    public function order() {
        return $this->hasOne(Order::class);
    }
}
