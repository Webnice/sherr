<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Illuminate\Database\Eloquent\Relations\BelongsTo;
use \Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Order.
 *
 * @package namespace App\Models;
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $latitude
 * @property string $longitude
 * @property string $type
 * @property int $category_id
 * @property int|null $sub_category_id
 * @property int $cost_day
 * @property int $safe_deal
 * @property int $insurance
 * @property int|null $sum_insurance
 * @property int $request_passport
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Category $category
 * @property-read \App\Models\SubCategory|null $subCategory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCostDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereRequestPassport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereSafeDeal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereSubCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereSumInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDescription($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderTenant[] $orderTenants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderPhotos[] $orderPhotos
 * @property-read int|null $order_tenants_count
 */
class Order extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'orders';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'user_id',
        'latitude',
        'longitude',
        'type',
        'category_id',
        'sub_category_id',
        'cost_day',
        'safe_deal',
        'insurance',
        'sum_insurance',
        'request_passport',
    ];

    /**
     * @return BelongsTo
     */
    public function category() {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function subCategory() {
        return $this->belongsTo(SubCategory::class);
    }

    /**
     * @return belongsToMany
     */
    public function orderTenants() {
        return $this->belongsToMany(OrderTenant::class);
    }

    /**
     * @return hasMany
     */
    public function orderPhotos() {
        return $this->hasMany(OrderPhotos::class);
    }
}
