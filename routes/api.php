<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/register', 'UserController@register')->middleware('guest');
Route::get('/category', 'CategoryController');
Route::get('/sub-category', 'SubCategoriesController@index');
Route::get('/orders', 'OrdersController@index');
Route::get('/orders/{id}', 'OrdersController@show');

Route::group(['middleware' => 'auth:api'], function () {
    //User routes
    Route::get('/user/details', 'UserController@details');
    Route::get('/user/list', 'UserController@list');
    Route::post('/user/avatar', 'UserController@avatar');
    Route::post('/user/verify', 'UserController@verify');
    Route::match(['get', 'put'], '/user/{user}/edit', 'UserController@edit');
    Route::post('/orders', 'OrdersController@store');
    Route::post('/orders/tenant', 'OrdersController@tenant');
    Route::put('/orders/tenant-status/{id}', 'OrdersController@updateTenantStatus');

    Route::get('/chats', 'OrdersController@chats');
    Route::get('/chats/{id}', 'OrdersController@chatsDetail');
    Route::post('/chat', 'OrdersController@chat');


    // Controllers Within The "App\Http\Controllers\Admin" Namespace
    Route::namespace('Admin')->group(function () {
        Route::resource('categories', 'CategoriesController')->except(['create']);
        Route::resource('sub-categories', 'SubCategoriesController')->except(['create', 'show']);
    });
});

