require('./bootstrap');

window.Vue = require('vue');
import router from './router';
import { store } from './store'

import App from './App.vue'
import VueBodyClass from 'vue-body-class';
import VueNotifications from 'vue-notifications';
import swal from 'sweetalert';

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('content-header',
    () => import('./components/partials/ContentHeader')
);

const vueBodyClass = new VueBodyClass(router.options.routes);
router.beforeEach((to, from, next) => { vueBodyClass.guard(to, next) });

function toast ({title, message, type, buttons}) {
    return swal(title, message, type, {
        buttons: buttons
    })
}

const options = {
    success: toast,
    error: toast,
    info: toast,
    warn: toast
};

Vue.use(VueNotifications, options);

const app = new Vue({
    el: '#app',
    components: {App},
    store,
    router
});


