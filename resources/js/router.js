import Vue    from 'vue';
import Router from 'vue-router';
import Home   from './components/Home';
import Login  from './components/layouts/LoginContent';
import UsersList from './components/user/List';
import User from './components/user/User';
import UserEdit from './components/user/Edit';
import Category from "./components/category/Category";
import CategoryList from "./components/category/List";
import CategoryStore from "./components/category/Store";
import SubCategory from "./components/sub-category/SubCategory";
import SubCategoryList from "./components/sub-category/List";
import SubCategoryStore from "./components/sub-category/Store";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                bodyClass: 'hold-transition login-page',
            }
        },
        {
            path: '/user',
            name: 'user',
            component: User,
            children: [
                {
                    path: 'list',
                    name: 'user-list',
                    component: UsersList
                },
                {
                    path: ':id/edit',
                    name: 'user-edit',
                    component: UserEdit
                }
            ]
        },
        {
            path: '/category',
            name: 'category',
            component: Category,
            children: [
                {
                    path: 'list',
                    name: 'category-list',
                    component: CategoryList
                },
                {
                    path: 'create',
                    name: 'category-create',
                    component: CategoryStore
                },
                {
                    path: ':id/edit',
                    name: 'category-edit',
                    component: CategoryStore
                }
            ]
        },
        {
            path: '/sub-category',
            name: 'sub-category',
            component: SubCategory,
            children: [
                {
                    path: 'list/:category',
                    name: 'sub-category-list',
                    component: SubCategoryList
                },
                {
                    path: 'create/:category',
                    name: 'sub-category-create',
                    component: SubCategoryStore
                },
                {
                    path: 'edit/:subCategory/:category',
                    name: 'sub-category-edit',
                    component: SubCategoryStore
                }
            ]
        }
    ],
    linkActiveClass: "active"
});