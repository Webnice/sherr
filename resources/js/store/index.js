'use strict';

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        layout: 'app-layout'
    },
    mutations: {
        setLayout (state, payload) {
            state.layout = payload
        }
    },
    getters: {
        layout (state) {return state.layout},
    }
});